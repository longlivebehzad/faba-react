import './App.css';
import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import NotFound from './component/notFound';
import { Layout } from 'antd';
import ElectionHeader from './component/header';
import 'react-toastify/dist/ReactToastify.css';
import Welcome from './component/welcome';
import Store from './component/store';
import Verify from './component/verify';
import SignUp from './component/signUp';
import SignIn from './component/signIn';
import Panel from './component/panel';
import History from './component/history';

const { Footer, Content } = Layout;

class App extends Component {
  state = {
    contract: {},
    currentAddress: null,
    isAdmin: false,
  };

  async componentDidMount() {
    var obj = setInterval(async () => {
      clearInterval(obj);

      if (!window.tronWeb) {
        alert('جهت استفاده از این پلتفرم کیف پول tronweb را نصب کنید.');
      }

      window.addEventListener('message', async (e) => {
        if (e.data.message && e.data.message.action == 'tabReply') {
          console.log('tabReply event', e.data.message);
          // if (e.data.message.data.data.node.chain == '_') {
          //   console.log('tronLink currently selects the main chain');
          // } else {
          //   console.log('tronLink currently selects the side chain');
          // }
        }

        if (e.data.message && e.data.message.action == 'setAccount') {
          console.log('current address:', e.data.message.data.address);
          this.setState({ currentAddress: e.data.message.data.address });
        }
        if (e.data.message && e.data.message.action == 'setNode') {
          console.log('setNode event', e.data.message);
          if (e.data.message.data.node.chain == '_') {
            console.log('tronLink currently selects the main chain');
          } else {
            console.log('tronLink currently selects the side chain');
          }
        }
      });
    }, 500);
  }

  render() {
    const { contract, currentAddress, isAdmin } = this.state;
    return (
      <Layout>
        <ToastContainer />
        <ElectionHeader isAdmin={isAdmin} currentAddress={currentAddress}></ElectionHeader>
        <Content style={{ minHeight: '100vh' }}>
          <Switch>
            {/* <Route
              exact
              path="/panel"
              render={(props) => (
                <Panel {...props} isAdmin={isAdmin} currentAddress={currentAddress} contract={contract} />
              )}
            /> */}

            <Route
              path="/panel"
              render={({ match: { url } }) => (
                <>
                  <Route path={`${url}/`} component={History} exact />
                  <Route path={`${url}/store`} component={Store} />
                  <Route path={`${url}/verify`} component={Verify} />
                </>
              )}
            />

            <Route
              exact
              path="/signin"
              render={(props) => (
                <SignIn {...props} isAdmin={isAdmin} currentAddress={currentAddress} contract={contract} />
              )}
            />
            <Route
              exact
              path="/signup"
              render={(props) => (
                <SignUp {...props} isAdmin={isAdmin} currentAddress={currentAddress} contract={contract} />
              )}
            />

            <Route exact path="/not-found" component={NotFound} />
            <Redirect from="/" to="/panel" />

            <Redirect to="not-found" />
          </Switch>
        </Content>
        <Footer style={{ textAlign: 'center', backgroundColor: '#1b2229', color: 'white' }}>
          ۱۳۹۹© - پلتفرم رای گیری روی بلاکچین
        </Footer>
      </Layout>
    );
  }
}

export default App;
