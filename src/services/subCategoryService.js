import http from './httpService';
import { getJwt } from './authService';

const subCategoryEndPoint = `/subcategory`;

export const createSubCategory = async (data) => {
  const { name, category } = data;
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.post(
    `${subCategoryEndPoint}`,
    {
      name,
      category,
    },
    config
  );

  return result;
};

export const getSubCategories = async (categoryId) => {
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.get(`${subCategoryEndPoint}/${categoryId}`, config);

  return result;
};

export default {
  createSubCategory,
};
