import http from './httpService';
import { getJwt } from './authService';
const hashEndPoint = `/hash`;

export const createHash = async (data) => {
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.post(`${hashEndPoint}`, data, config);

  return result;
};

export const getHashes = async () => {
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.get(`${hashEndPoint}`, config);

  return result;
};

export default {
  createHash,
  getHashes,
};
