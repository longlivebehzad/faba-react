import http from './httpService';
import { getJwt } from './authService';
const categoryEndPoint = `/category`;
const tokenKey = 'token';

export const createCategory = async (data) => {
  const { name } = data;
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.post(
    `${categoryEndPoint}`,
    {
      name,
    },
    config
  );

  return result;
};

export const getCategory = async () => {
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.get(`${categoryEndPoint}`, config);

  return result;
};

export default {
  createCategory,
  getCategory,
};
