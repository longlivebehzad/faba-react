import http from './httpService';
import jwtDecode from 'jwt-decode';

const authEndPoint = `/auth`;
const tokenKey = 'token';

export const signin = async (data) => {
  const { email, password } = data;
  const result = await http.post(`${authEndPoint}/signin`, {
    email,
    password,
  });
  localStorage.setItem(tokenKey, result.data.accessToken);

  return result;
};

export const signup = async (data) => {
  const { email, password, name } = data;
  const result = await http.post(`${authEndPoint}/signup`, {
    email,
    password,
    name,
  });

  localStorage.setItem(tokenKey, result.data.accessToken);
  return result;
};

export const logout = () => {
  localStorage.removeItem(tokenKey);
};

export const jwtIsExpired = () => {
  const jwt = localStorage.getItem(tokenKey);
  if (!jwt) return true;
  const exp = jwtDecode(jwt).exp;

  return exp < Date.now() / 1000;
};

export const getCurrentUser = () => {
  try {
    const jwt = localStorage.getItem(tokenKey);
    const user = jwtDecode(jwt);
    return user;
  } catch (ex) {
    return null;
  }
};

export const getJwt = () => {
  return localStorage.getItem(tokenKey);
};

export const getUserInfo = async () => {
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.get(`${authEndPoint}/user-info`, config);

  return result;
};

export const update = async (data) => {
  const config = {
    headers: { Authorization: `Bearer ${getJwt()}` },
  };
  const result = await http.put(`${authEndPoint}`, data, config);

  localStorage.setItem(tokenKey, result.data.accessToken);
  return result;
};

http.setJwt(getJwt());

export default {
  signin,
  signup,
  logout,
  getCurrentUser,
  getJwt,
  jwtIsExpired,
  getUserInfo,
  update,
};
