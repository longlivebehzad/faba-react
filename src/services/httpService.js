import axios from 'axios';
import { toast } from 'react-toastify';

axios.interceptors.response.use(null, failure);
axios.defaults.baseURL = process.env.REACT_APP_API_URL;

function failure(error) {
  const expectedError = error.response && error.response.status >= 400 && error.response.status <= 500;

  if (!expectedError) {
    console.log(error);

    toast.error('An unexpected error occurred.');
  }

  return Promise.reject(error);
}

function setJwt(jwt) {
  axios.defaults.headers.common['x-auth-token'] = jwt;
}
export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  patch: axios.patch,
  delete: axios.delete,
  setJwt,
};
