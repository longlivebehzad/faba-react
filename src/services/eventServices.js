import http from './httpService';
const eventEndPoint = `https://api.trongrid.io/v1/transactions`;

export const getEventByTransactionId = (txId) => {
  return http.get(`${eventEndPoint}/${txId}/events`);
};
