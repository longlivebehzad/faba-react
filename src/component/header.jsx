import React, { Component, useState, useEffect } from 'react';
import { Layout, Menu } from 'antd';
import { MailOutlined, UserOutlined, HomeOutlined, UserSwitchOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { getCurrentUser, logout } from '../services/authService';

const { Header } = Layout;
const { SubMenu } = Menu;

const ElectionHeader = ({ currentAddress, isAdmin }) => {
  const [currentItem, setCurrentItem] = useState('Home');

  const [user, setUser] = useState(null);

  useEffect(async () => {
    const user = await getCurrentUser();
    if (user) return setUser(user.name);
  }, []);

  const handleClick = (e) => {
    setCurrentItem(e.key);
  };
  const handleLogout = () => {
    logout();
    window.location = '/';
  };

  return (
    <Header className="election-header">
      <Menu onClick={handleClick} selectedKeys={[currentItem]} mode="horizontal">
        {user ? (
          <SubMenu key="SubMenu" icon={<UserOutlined />} title={`${user}`}>
            <Menu.Item onClick={handleLogout} key="setting:1">
              خروج
            </Menu.Item>
          </SubMenu>
        ) : null}

        {currentAddress ? (
          <Menu.Item key="currentAddress" icon={<UserSwitchOutlined />}>
            <span>{currentAddress ? <span>{currentAddress}</span> : ''}</span>
          </Menu.Item>
        ) : null}
      </Menu>
    </Header>
  );
};

export default ElectionHeader;
