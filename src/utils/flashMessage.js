import { toast } from 'react-toastify';

export default (message, type = 'info') => {
  toast[type](message);
};
