import * as TronWeb from 'tronweb';

function tronweb() {
  const tronWeb = new TronWeb({
    fullHost: process.env.REACT_APP_FULL_HOST,
    privateKey: process.env.REACT_APP_PRIVATE_KEY,
  });

  return tronWeb;
}

export const convertToHex = async (address) => {
  const tw = await tronweb();
  return tw.address.fromHex(address);
};

export default tronweb;
